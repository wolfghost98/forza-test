import React from 'react';
import './App.css';
import { DataGrid, SearchPanel } from 'devextreme-react/data-grid';
import { Modal, Button } from "react-bootstrap";
import Swal from 'sweetalert2'
import axios from 'axios';


const columns = ['Codigo', 'Marca', 'Descripcion', 'PrecioUnitario', 'Stock', 'Fecha_Abastecimiento', "Cantidad_Abastecimiento"];


class App extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
      productos: [],
      url: "http://localhost:5000/api/inventario/getAll",
      total: 0,
      promedio: 0,
      nombre: '',
      nit: '',
      direccion: '',
      tipo: 'Venta',
      id: '',
      cantidad: 0,
      carro: [],
      total: 0
    }


    this.getData = this.getData.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.handleShow = this.handleShow.bind(this)
    this.addProduct = this.addProduct.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.addVenta = this.addVenta.bind(this)
    this.addCompra = this.addCompra.bind(this)
    this.cleanModal = this.cleanModal.bind(this)
  }

  componentDidMount() {
    document.title = "Forza Test"
    this.getData()

  }




  getData() {
    fetch(this.state.url)
      .then(response => response.json())
      .then(data => {
        data = data['msg']
        data = data.map((i) =>{
          let fecha = i.Fecha_Abastecimiento.split('T')[0]
          let hora = i.Fecha_Abastecimiento.split("T")[1].split(".")["0"]
          i.Fecha_Abastecimiento = fecha + " " + hora
          return i
        })
        this.setState({ productos: data })
        this.setState({ total: 0 })
        this.setState({ total: data.length })
        let aux = 0
        data.forEach(element => {
          aux += element.PrecioUnitario
        });
        this.setState({ promedio: (aux / data.length).toFixed(2) })

      })
      .catch(err => {
        console.error(err)
        this.setState({ productos: [] })
      });
  }


  handleShow() {
    this.setState({ show: true })
  }


  handleClose() {
    this.setState({ show: false })
    this.cleanModal()
  }

  addProduct() {
    if (this.state.cantidad <= 0) {
      Swal.fire({
        title: 'Error!',
        text: "La cantidad tiene que ser mayor a 0",
        icon: 'error',
      })
      return;
    }
    let product = this.state.productos.filter(producto => producto.Codigo == this.state.id)
    if (product.length === 0) {
      Swal.fire({
        title: 'Error!',
        text: "No se encuentra el Producto",
        icon: 'error',
      })
      return;
    }


    let aux = this.state.carro.filter(producto => producto.id == product[0].Codigo)
    if (aux.length > 0) aux[0].cantidad += +this.state.cantidad;
    else this.state.carro.push(
      {
        id: product[0].IdInventario,
        cantidad: +this.state.cantidad,
        marca: product[0].Marca,
        precio: product[0].PrecioUnitario
      })

    let total = this.state.carro.reduce((a, b) => {
      a += (+b.cantidad * (+b.precio))
      return a;
    }, 0)
    this.setState({ total: total })
    this.setState({ cantidad: 0 })
    this.setState({ id: '' })
    //console.log(product)
  }

  cleanModal() {
    this.setState({ total: 0 })
    this.setState({ carro: [] })
    this.setState({ nombre: '' })
    this.setState({ direccion: '' })
    this.setState({ nit: '' })
    this.setState({ tipo: 'Venta' })
    this.setState({ cantidad: 0 })
    this.setState({ id: '' })
  }

  onSubmit(e) {
    e.preventDefault()
    if (this.state.carro.length <= 0) {
      Swal.fire({
        title: 'Error!',
        text: "Agregue un Producto",
        icon: 'error',
      })
      return;
    }
    let data = {
      nombre: this.state.nombre,
      nit: this.state.nit,
      direccion: this.state.direccion
    }

    let productos = {
      id: 0,
      productos: this.state.carro
    }
    if (this.state.tipo === 'Venta') this.addVenta(data, productos)
    else this.addCompra(data, productos)
  }

  addVenta(data, productos) {
    axios.post('http://localhost:5000/api/venta/addVenta', data)
      .then(res => {
        //console.log(res);
        //console.log(res.data);
        if (res.data.status === 400) {
          Swal.fire({
            title: 'Error!',
            text: res.data.msg,
            icon: 'error',
          })
          return;
        }
        let idVenta = res.data.msg
        productos.id = idVenta
        axios.post('http://localhost:5000/api/venta/updateStock', productos)
          .then(res => {
            if (res.data.status === 400) {
              Swal.fire({
                title: 'Error!',
                text: res.data.msg,
                icon: 'error',
              })
              return;
            }
            Swal.fire({
              title: 'Success!',
              text: "Venta Realizada",
              icon: 'success',
            })
            this.cleanModal()
            this.getData()
          }).catch(error => {
            console.log(error.response)
            Swal.fire({
              title: 'Error!',
              text: 'Ha ocurrido un error:(',
              icon: 'error',
            })
          })



      }).catch(error => {
        console.log(error.response)
        Swal.fire({
          title: 'Error!',
          text: 'Ha ocurrido un error:(',
          icon: 'error',
        })
      })
  }


  addCompra(data, productos) {
    axios.post('http://localhost:5000/api/compra/addCompra', data)
      .then(res => {
        //console.log(res);
        //console.log(res.data);
        if (res.data.status === 400) {
          Swal.fire({
            title: 'Error!',
            text: res.data.msg,
            icon: 'error',
          })
          return;
        }
        let idVenta = res.data.msg
        productos.id = idVenta
        axios.post('http://localhost:5000/api/compra/updateStock', productos)
          .then(res => {
            if (res.data.status === 400) {
              Swal.fire({
                title: 'Error!',
                text: res.data.msg,
                icon: 'error',
              })
              return;
            }
            Swal.fire({
              title: 'Success!',
              text: "Compra Realizada",
              icon: 'success',
            })
            this.cleanModal()
            this.getData()
          }).catch(error => {
            console.log(error.response)
            Swal.fire({
              title: 'Error!',
              text: 'Ha ocurrido un error:(',
              icon: 'error',
            })
          })



      }).catch(error => {
        console.log(error.response)
        Swal.fire({
          title: 'Error!',
          text: 'Ha ocurrido un error:(',
          icon: 'error',
        })
      })
  }
  handleInputChange(e) {
    let { id, value } = e.target
    if (id === "tipo") this.setState({ tipo: value })
    else if (id === "nombre") this.setState({ nombre: value })
    else if (id === "nit") this.setState({ nit: value })
    else if (id === "direccion") this.setState({ direccion: value })
    else if (id === "codigo") this.setState({ id: value })
    else if (id === "cantidad") this.setState({ cantidad: value })
    //console.log(value)
  }



  render() {
    return (
      <div className="example">

        <div className="toolbar container-fluid">
          <div className="row">
            <div className="col-lg-6 col-sm-6 col-12">
              <b>Total: </b> {this.state.total}
            </div>
            <div className="col-lg-3 col-sm-3 col-12">
              <b>Promedio: </b> {this.state.promedio}
            </div>
            <div className="col-lg-3 col-sm-3 col-12">
              <button onClick={() => this.handleShow()} className="btn btn-primary">Transaccion</button>
            </div>
          </div>

        </div>
        <DataGrid

          dataSource={this.state.productos}
          defaultColumns={columns}
          showBorders={true}
        >

          <SearchPanel visible={true} highlightCaseSensitive={true} />
        </DataGrid>


        <div>
          <Modal show={this.state.show} onHide={this.handleClose} animation={false} className="mt-5" size="lg">
            <Modal.Header closeButton>
              <Modal.Title className="text-dark">Transaccion</Modal.Title>
            </Modal.Header>
            <Modal.Body className="text-dark">
              <form onSubmit={this.onSubmit}>
                <div class="form-group">
                  <label for="tipo">Tipo</label>
                  <select class="form-control" id="tipo" value={this.state.tipo} onChange={this.handleInputChange}>
                    <option>Venta</option>
                    <option>Compra</option>
                  </select>
                </div>
                <div className="form-group">
                  <label for="nombre">Nombre Completo</label>
                  <input type="text" class="form-control" id="nombre" value={this.state.nombre} onChange={this.handleInputChange} placeholder="Ingresar Nombre Completo" required />
                </div>
                <div className="form-group">
                  <label for="nit">NIT</label>
                  <input type="text" class="form-control" id="nit" value={this.state.nit} onChange={this.handleInputChange} placeholder="Ingresar NIT" required />
                </div>
                <div className="form-group">
                  <label for="direccion">Direccion</label>
                  <input type="text" class="form-control" id="direccion" value={this.state.direccion} onChange={this.handleInputChange} placeholder="Ingresar Direccion" required />
                </div>
                <div className="form-group row">
                  <div class="col">
                    <input type="text" id="codigo" value={this.state.id} onChange={this.handleInputChange} class="form-control" placeholder="Codigo" />
                  </div>
                  <div class="col">
                    <input type="number" id="cantidad" value={this.state.cantidad} onChange={this.handleInputChange} class="form-control" placeholder="Cantidad" />
                  </div>
                  <div class="col">
                    <button type="button" onClick={() => this.addProduct()} className="btn btn-primary">Agregar</button>
                  </div>
                </div>
                <div className="form-group">
                  <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">Codigo</th>
                        <th scope="col">Marca</th>
                        <th scope="col">Cantidad</th>
                        <th scope="col">Precio</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.carro.map((producto, i) => (
                        <tr key={i}>
                          <th >{producto.id}</th>
                          <td>{producto.marca}</td>
                          <td>{producto.cantidad}</td>
                          <td>{producto.precio}</td>
                        </tr>
                      ))}

                    </tbody>
                  </table>

                </div>
                <div className="form-group">
                  <label ><b>Total: </b> {this.state.total}</label>
                </div>
                <div className="form-group">
                  <input type="submit" className="btn btn-primary btn-lg btn-block" value="Guardar" />
                </div>
              </form>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={this.handleClose}>
                Close
              </Button>
            </Modal.Footer>
          </Modal>
        </div>
      </div>
    );
  }
}

export default App;