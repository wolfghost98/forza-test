const db = require('../db/database')

const addCompra = async (req, res) => {
    const { nombre, nit, direccion } = req.body
    let sql = `INSERT INTO Compra(Nombre,Nit,Direccion) VALUES('${nombre}','${nit}','${direccion}');
    SELECT SCOPE_IDENTITY() as id;`

    db.query(sql, (err, result) => {
        if (err) {
            console.log(err)
            return res.json({
                status: 400,
                msg: err.sqlMessage
            })
        }
        let idCompra = result["recordset"][0].id
        return res.json({
            status: 200,
            msg: idCompra
        })


    })
}

const updateStock = async (req, res) => {
    const { id, productos } = req.body
    let sql = `SELECT Stock FROM Inventario WHERE `;
    for (let i = 0; i < productos.length; i++) {
        if (i === 0) sql += ` IdInventario = '${productos[i].id}' `
        else sql += ` OR IdInventario = '${productos[i].id}' `
    }
    db.query(sql, (err, result) => {
        if (err) {
            console.log(err)
            return res.json({
                status: 400,
                msg: err.sqlMessage
            })
        }
        let data = result["recordset"]
        sql = ``
        for (let i = 0; i < productos.length; i++) {
            sql += `UPDATE Inventario SET Stock = ${data[i].Stock + productos[i].cantidad},
            Fecha_Abastecimiento = '${new Date().toISOString().slice(0, 19).replace('T', ' ')}', Cantidad_Abastecimiento = ${productos[i].cantidad} 
            WHERE IdInventario = ${productos[i].id}; 
            
            INSERT INTO Detalle_Compra(IdCompra,IdInventario,Cantidad) VALUES(${id},${productos[i].id},${productos[i].cantidad}); 
            `
        }

        db.query(sql, (err, result) => {
            if (err) {
                console.log(err)
                return res.json({
                    status: 400,
                    msg: err.sqlMessage
                })
            }

            return res.json({
                status: 200,
                msg: result
            })
        })

    })

}


module.exports = {
    updateStock,
    addCompra
}