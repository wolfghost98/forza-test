const db = require('../db/database')

const getAll = async(req,res) =>{
    
    let sql = `SELECT * 
               FROM Inventario
               `;
    db.query(sql,(err,result) =>{
        if(err){
            console.log(err)
            return res.json({
                status: 400,
                msg : err.sqlMessage
            })
        }
        return res.json({
            status: 200,
            msg : result["recordset"]
        })
    })
    
}


module.exports = {
    getAll
}