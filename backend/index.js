const express = require('express');
const cors = require('cors');
const PORT = process.env.PORT || 5000;

const app = express();

//Middlewares
app.use(cors());
app.use(express.json());


//Routes
app.use('/api/inventario',require('./routes/inventario.route'));
app.use('/api/compra',require('./routes/compra.route'));
app.use('/api/venta',require('./routes/venta.route'));

app.listen(PORT,() => {
    console.log('Server running on port', PORT);
})