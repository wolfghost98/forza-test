CREATE DATABASE forzaTest;
use  forzaTest;

CREATE TABLE Inventario (
  IdInventario              INT IDENTITY(1,1)  NOT NULL,
  Marca                     VARCHAR(45)        NOT NULL,
  Descripcion               VARCHAR(100)       NOT NULL,
  Codigo                    VARCHAR(30)        NOT NULL,
  PrecioUnitario            DECIMAL(10,2)      NOT NULL,
  Stock                     INT                NOT NULL,
  Fecha_Abastecimiento      DATETIME          NOT NULL,
  Cantidad_Abastecimiento   INT                NOT NULL,
  PRIMARY KEY(IdInventario)
);


CREATE TABLE  Compra(
  IdCompra                  INT IDENTITY(1,1)  NOT NULL,
  Nombre                    VARCHAR(100)       NOT NULL,
  Nit                       VARCHAR(30)        NOT NULL,
  Direccion                 VARCHAR(100)       NOT NULL,
  Fecha                     DATETIME DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(IdCompra),
);

CREATE TABLE  Venta(
  IdVenta                  INT IDENTITY(1,1)  NOT NULL,
  Nombre                    VARCHAR(100)       NOT NULL,
  Nit                       VARCHAR(30)        NOT NULL,
  Direccion                 VARCHAR(100)       NOT NULL,
  Fecha                    DATETIME DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(IdVenta),
);


CREATE TABLE  Detalle_Compra(
  Id                        INT IDENTITY(1,1)  NOT NULL,
  IdCompra                  INT                NOT NULL,
  IdInventario              INT                NOT NULL,
  Cantidad                  INT                NOT NULL,
  PRIMARY KEY(Id),
  FOREIGN KEY(IdCompra)     REFERENCES Compra(IdCompra),
  FOREIGN KEY(IdInventario) REFERENCES Inventario(IdInventario),
);


CREATE TABLE  Detalle_Venta(
  Id                        INT IDENTITY(1,1)  NOT NULL,
  IdVenta                   INT                NOT NULL,
  IdInventario              INT                NOT NULL,
  Cantidad                  INT                NOT NULL,
  PRIMARY KEY(Id),
  FOREIGN KEY(IdVenta)      REFERENCES Venta(IdVenta),
  FOREIGN KEY(IdInventario) REFERENCES Inventario(IdInventario),
);

